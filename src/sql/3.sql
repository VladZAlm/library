INSERT INTO ui_text (name,value) VALUES ('add.book.fail.msg','Не удалось добавить книгу');
INSERT INTO ui_text (name,value) VALUES ('delete.book.confirm.msg','Удалить книгу?');
INSERT INTO ui_text (name,value) VALUES ('delete.book.fail.msg','Не удалось удалить книгу');
INSERT INTO ui_text (name,value) VALUES ('editor.add.header','Добавление книги');
INSERT INTO ui_text (name,value) VALUES ('editor.edit.header','Редактирование книги');
INSERT INTO ui_text (name,value) VALUES ('need.book.date.msg','Введите дату');
INSERT INTO ui_text (name,value) VALUES ('need.book.name.msg','Введите название');
INSERT INTO ui_text (name,value) VALUES ('save.book.fail.msg','Не удалось сохранить изменения');
CREATE TABLE book (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(150) NOT NULL,
    author VARCHAR(150),
    release_date DATE NOT NULL,
    is_public BOOLEAN NOT NULL
);
CREATE INDEX ON book (is_public);
CREATE INDEX ON book (name);

CREATE TABLE ui_text (
  name VARCHAR(50) PRIMARY KEY,
  value VARCHAR(1000) NOT NULL
);

CREATE TABLE log (
	id BIGSERIAL PRIMARY KEY,
	code VARCHAR(100),
	date TIMESTAMP,
	logger VARCHAR(300),
	level VARCHAR(100),
	message VARCHAR(7000)
);
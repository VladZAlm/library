import { BookEditManager } from "./book-edit-manager.js";

function TextsClass() {
    this.SERVER_CONNECTION_FAIL_MSG = 'Не удалось подключиться к серверу';
    this.PUBLIC_CATALOG_URL;
    this.PRIVATE_CATALOG_URL;
    this.GET_BOOKS_URL;
    this.SAVE_BOOK_URL;
    this.ADD_BOOK_URL;
    this.DELETE_BOOK_URL;
    this.SAVE_BOOK_FAIL_MSG;
    this.ADD_BOOK_FAIL_MSG;
    this.DELETE_BOOK_FAIL_MSG;
    this.NEED_BOOK_NAME_MSG;
    this.NEED_BOOK_DATE_MSG;
    this.DELETE_BOOK_CONFIRM_MSG;
    this.EDITOR_EDIT_HEADER;
    this.EDITOR_ADD_HEADER;

    this.init = function (onDone) {
        let context = this;
        BookEditManager.getTexts(function (res) {
            context.PUBLIC_CATALOG_URL = res.publicCatalogUrl;
            context.PRIVATE_CATALOG_URL = res.privateCatalogUrl;
            context.GET_BOOKS_URL = res.getBooksUrl;
            context.SAVE_BOOK_URL = res.saveBookUrl;
            context.ADD_BOOK_URL = res.addBookUrl;
            context.DELETE_BOOK_URL = res.deleteBookUrl;
            context.SAVE_BOOK_FAIL_MSG = res.saveBookFailMsg;
            context.ADD_BOOK_FAIL_MSG = res.addBookFailMsg;
            context.DELETE_BOOK_FAIL_MSG = res.deleteBookFailMsg;
            context.NEED_BOOK_NAME_MSG = res.needBookNameMsg;
            context.NEED_BOOK_DATE_MSG = res.needBookDateMsg;
            context.DELETE_BOOK_CONFIRM_MSG = res.deleteBookConfirmMsg;
            context.EDITOR_EDIT_HEADER = res.editorEditHeader;
            context.EDITOR_ADD_HEADER = res.editorAddHeader;
            onDone();
        })
    }
}

export const Texts = new TextsClass();
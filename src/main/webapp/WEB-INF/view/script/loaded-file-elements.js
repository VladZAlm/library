import { Page } from "./page.js";

function LoadedFileElementsClass() {
    let bookEditor;
    let bookTableRow;

    this.init = function (onDone) {
        $.when(
            $.get('components/book-editor.html', function (res) {
                bookEditor = getElementFromHtmlString(res);
            }),
            $.get('components/book-table-row.html', function (res) {
                bookTableRow = getElementFromHtmlString(res);
            })
        ).done(function () {
            onDone();
        });
    }

    let getElementFromHtmlString = function (htmlString) {
        let container = $('<div/>').append($.parseHTML(htmlString)).find('.container');
        let res = $('<div/>');
        res.append(container.children());
        return res;
    }

    this.getBookEditor = function () {
        return bookEditor.clone();
    }

    this.getBookTableRow = function () {
        return bookTableRow.clone();
    }
}

export const LoadedFileElements = new LoadedFileElementsClass();
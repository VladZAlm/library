function CatalogViewChangerClass() {
	let publicButton = $('.public-catalog-button');
	let privateButton = $('.private-catalog-button');
	let inactiveClass = 'catalog-type-button_inactive';
	let publicColor = 'white';
	let privateColor = '#acccff';

	this.setCtalogView = function (isPublic) {
		if (isPublic) {
			setButtonsView(publicButton, privateButton);
		} else {
			setButtonsView(privateButton, publicButton);
		}
		setBackgroundColor(isPublic);
	};

	let setButtonsView = function (activeButton, inactiveButton) {
		activeButton.removeClass(inactiveClass);
		inactiveButton.addClass(inactiveClass);
	};

	let setBackgroundColor = function (isPublic) {
		let colorToSet;
		if (isPublic) {
			colorToSet = publicColor;
		} else {
			colorToSet = privateColor;
		}

		$('body').css('background-color', colorToSet)
	}
}

export const CatalogViewChanger = new CatalogViewChangerClass();
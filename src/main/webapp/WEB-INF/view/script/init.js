import { LoadedFileElements } from './loaded-file-elements.js';
import { Texts } from "./texts.js";
import { Page } from "./page.js";

LoadedFileElements.init(function () {
    Texts.init(function () {
        Page.start();
    });
});
function JsUtilsClass() {
	this.checkIsDefined = function (obj) {
		return !((typeof obj === 'undefined') || obj === null);
	}
	this.chechIsBlank = function (s) {
		return !this.checkIsDefined(s) || this.checkIsEmpty(s);
	}
	this.checkIsEmpty = function (s) {
		return !s.trim()
	}
}

export const JsUtils = new JsUtilsClass();
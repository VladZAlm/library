function SystemUtilsClass() {
	this.getUrlParam = function (name) {
		var results = new RegExp('[\?&]' + name + '=([^&#]*)')
			.exec(window.location.search);
		return (results !== null) ? results[1] || 0 : null;
	}
}

export const SystemUtils = new SystemUtilsClass();
import { JsUtils } from './js-utils.js';

function ElementsUtilsClass() {
	this.setVisibility = function (element, visibility, displayValue) {
		let cssValue = 'display';
		let defaultCssVisible = 'block';
		let defaultCssInvisible = 'none';
		if (visibility) {
			if (JsUtils.checkIsDefined(displayValue)) {
				element.css(cssValue, displayValue);
				return;
			}
			element.css(cssValue, defaultCssVisible);
			return;
		}
		element.css(cssValue, defaultCssInvisible);
	}
}

export const ElementsUtils = new ElementsUtilsClass();
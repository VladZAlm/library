import { BookTableRow } from './book-table-row.js';
import { Page } from "./page.js";
import { BookEditManager } from "./book-edit-manager.js";
import { ElementsUtils } from './utils/elements-utils.js';

function TableManagerClass() {
    let tableRows = [];

    this.fillTable = function (isPublic) {
        BookEditManager.get(isPublic, function (res) {
            $.each(res, function (index, book) {
                let id = book.id;
                let name = book.name;
                let author = book.author;
                let date = new Date(book.releaseDate);
                addTableRow(id, name, author, date, true);
            });
            setVisibility(res.length != 0);
        });
    };

    let addTableRow = function (id, name, author, date, append) {
        let onDelete = function () {
            deleteRow(id);
        };
        let bookTableRow = new BookTableRow(id, name, author, date, onDelete);
        tableRows.push({ id: id, tableRow: bookTableRow });
        let element = bookTableRow.getContainer();
        let table = $('.books-table').find('tbody');
        if (append) {
            table.append(element);
        } else {
            let header = table.find('.books-table__header');
            element.insertAfter(header);
        }
    }

    let setVisibility = function (visibility) {
        ElementsUtils.setVisibility($('.books-table'), visibility, 'table');
    }

    this.applyBookChange = function (isPublic, id, name, author, date) {
        if (isPublic !== Page.getIsPublic()) {
            deleteRow(id);
            return;
        }

        let tableRow = getTableRow(id);
        tableRow.setName(name);
        tableRow.setAuthor(author);
        tableRow.setDate(date);
    }

    let deleteRow = function (id) {
        let tableRow = getTableRow(id);
        tableRows = $.grep(tableRows, function (e) {
            return e.id != id;
        });
        if (tableRows.length === 0) {
            setVisibility(false);
        }
        tableRow.getContainer().remove();
    }

    this.addBook = function (isPublic, id, name, author, date) {
        if (isPublic !== Page.getIsPublic()) {
            return;
        }
        addTableRow(id, name, author, date, false);
        setVisibility(true);
    };

    let getTableRow = function (id) {
        let res = null;
        $.each(tableRows, function (index, data) {
            if (id !== data.id) {
                return;
            }
            res = data.tableRow;
            return false;
        });
        return res;
    }
}

export const TableManager = new TableManagerClass();
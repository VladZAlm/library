export function BookEditorElements(element) {
    let container;
    let idElement;
    let headerElement;
    let publicCheckElement;
    let privateCheckElement;
    let nameElement;
    let authorElement;
    let dateElement;
    let cancelElement;
    let saveElement;

    {
        container = element;
        idElement = element.find('#book-editor__id');
        headerElement = element.find('.book-editor__header');
        publicCheckElement = element[0].querySelector('#book-editor__catalog-type-public');
        privateCheckElement = element[0].querySelector('#book-editor__catalog-type-private');
        nameElement = element.find('#book-editor__name');
        authorElement = element.find('#book-editor__author');
        dateElement = element.find('#book-editor__date');
        cancelElement = element.find('.book-editor__cancel-button');
        saveElement = element.find('.book-editor__save-button');
    }

    this.getContainer = function () {
        return container;
    }

    this.getIdElement = function () {
        return idElement;
    }

    this.getHeaderElement = function () {
        return headerElement;
    }

    this.getPublicCheckElement = function () {
        return publicCheckElement;
    }

    this.getPrivateCheckElement = function () {
        return privateCheckElement;
    }

    this.getNameElement = function () {
        return nameElement;
    }


    this.getAuthorElement = function () {
        return authorElement;
    }

    this.getDateElement = function () {
        return dateElement;
    }

    this.getCancelElement = function () {
        return cancelElement;
    }

    this.getSaveElement = function () {
        return saveElement;
    }
}
import { CatalogViewChanger } from './catalog-view-changer.js';
import { SystemUtils } from './utils/system-utils.js';
import { TableManager } from "./table-manager.js";
import { BookEditor } from "./book-editor.js";
import { Texts } from './texts.js';

function PageClass() {
    let isPublic;

    this.getIsPublic = function () {
        return isPublic;
    };

    this.start = function () {
        $("#public-catalog-button").attr("href", Texts.PUBLIC_CATALOG_URL);
        $("#private-catalog-button").attr("href", Texts.PRIVATE_CATALOG_URL);
        setCtalogView();
        setAddBookButtonOnClick();
        TableManager.fillTable(isPublic);
    };

    let setCtalogView = function () {
        let catalogParam = SystemUtils.getUrlParam('catalog');
        isPublic = catalogParam !== 'private';
        CatalogViewChanger.setCtalogView(isPublic);
    };

    let setAddBookButtonOnClick = function () {
        $('.add-book-button').on('click', function () {
            BookEditor.showToAdd(isPublic);
        });
    }
}

export const Page = new PageClass();
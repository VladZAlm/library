import { JsUtils } from './utils/js-utils.js';
import { ElementsUtils } from './utils/elements-utils.js';
import { LoadedFileElements } from './loaded-file-elements.js';
import { BookEditorElements } from "./book-editor-elements.js";
import { BookEditManager } from "./book-edit-manager.js";
import { TableManager } from "./table-manager.js";
import { Texts } from "./texts.js";

function BookEditorClass() {
	let bookEditorElements;

	this.showToEdit = function (id, isPublic, name, author, date) {
		fillBookEditor(id, isPublic, name, author, date);
		bookEditorElements.getHeaderElement().text(Texts.EDITOR_EDIT_HEADER);
		setSaveElementOnClickSave();
	};

	this.showToAdd = function (isPublic) {
		fillBookEditor(null, isPublic, "", "", null);
		bookEditorElements.getHeaderElement().text(Texts.EDITOR_ADD_HEADER);
		setSaveElementOnClickAdd();
	};

	let getBook = function () {
		let res = new Object();
		let strId = bookEditorElements.getIdElement().text();
		res.id = parseInt(strId);
		res.isPublic = getIsPublic();
		res.name = bookEditorElements.getNameElement().val();
		res.author = bookEditorElements.getAuthorElement().val();
		let elementDate = bookEditorElements.getDateElement().val();
		let dateToRes;
		if (JsUtils.chechIsBlank(elementDate)) {
			dateToRes = null;
		} else {
			dateToRes = new Date(elementDate).getTime();
		}
		res.releaseDate = dateToRes;
		return res;
	};

	let setVisibility = function (visibility) {
		ElementsUtils.setVisibility(bookEditorElements.getContainer(), visibility);
	};

	let fillBookEditor = function (id, isPublic, name, author, date) {
		if (!JsUtils.checkIsDefined(bookEditorElements)) {
			let element = LoadedFileElements.getBookEditor();
			bookEditorElements = new BookEditorElements(element);
			setCancelElementOnClick();
			$('body').append(bookEditorElements.getContainer());
		}

		let elementId;
		if (JsUtils.checkIsDefined(id)) {
			elementId = id;
		} else {
			elementId = "";
		}
		bookEditorElements.getIdElement().text(elementId);
		setCatalogType(isPublic);
		bookEditorElements.getNameElement().val(name);
		bookEditorElements.getAuthorElement().val(author);
		setDate(date);
		setVisibility(true);
	};

	let getIsPublic = function () {
		return bookEditorElements.getPublicCheckElement().checked;
	}

	let setCatalogType = function (isPublic) {
		let element;
		if (isPublic) {
			element = bookEditorElements.getPublicCheckElement();
		} else {
			element = bookEditorElements.getPrivateCheckElement();
		}
		element.checked = true;
	};

	let setDate = function (date) {
		if (!JsUtils.checkIsDefined(date)) {
			bookEditorElements.getDateElement().val("");
			return;
		}
		let day = ("0" + date.getDate()).slice(-2);
		let month = ("0" + (date.getMonth() + 1)).slice(-2);
		let value = date.getFullYear() + "-" + (month) + "-" + (day);
		bookEditorElements.getDateElement().val(value);
	};

	let setCancelElementOnClick = function () {
		bookEditorElements.getCancelElement().on('click', function () {
			setVisibility(false);
		});
	};

	let setSaveElementOnClickSave = function () {
		let onSuccess = function (book) {
			TableManager.applyBookChange(book.isPublic, book.id, book.name,
				book.author, new Date(book.releaseDate));
		};
		let action = function (book, onComplete) {
			BookEditManager.save(book, onSuccess, onComplete);
		};
		setSaveElementOnClick(action);
	};

	let setSaveElementOnClickAdd = function () {
		let onSuccess = function (book) {
			TableManager.addBook(book.isPublic, book.id, book.name,
				book.author, new Date(book.releaseDate));
		};
		let action = function (book, onComplete) {
			BookEditManager.add(book, onSuccess, onComplete);
		};
		setSaveElementOnClick(action);
	};

	let setSaveElementOnClick = function (action) {
		bookEditorElements.getSaveElement().unbind('click');
		bookEditorElements.getSaveElement().on('click', function () {
			let book = getBook();
			if (!validateBook(book)) {
				return;
			}
			let onComplete = function () {
				setVisibility(false);
			};
			action(book, onComplete);
		});
	};

	let validateBook = function (book) {
		if (JsUtils.checkIsEmpty(book.name)) {
			alert(Texts.NEED_BOOK_NAME_MSG);
			return false;
		}
		if (!JsUtils.checkIsDefined(book.releaseDate)) {
			alert(Texts.NEED_BOOK_DATE_MSG);
			return false;
		}
		return true;
	};
}

export const BookEditor = new BookEditorClass();
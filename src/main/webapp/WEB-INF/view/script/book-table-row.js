import { BookEditor } from './book-editor.js';
import { LoadedFileElements } from './loaded-file-elements.js';
import { Page } from "./page.js";
import { BookEditManager } from "./book-edit-manager.js";
import { Texts } from './texts.js';

export function BookTableRow(id, name, author, date, onDelete) {
    let container;
    let bookId;
    let nameElement;
    let authorElement;
    let dateElement;
    let bookDate;

    this.getContainer = function () {
        return container;
    }

    this.getName = function () {
        return nameElement.text();
    }

    this.setName = function (name) {
        nameElement.text(name);
    }

    this.setAuthor = function (author) {
        authorElement.text(author);
    }

    this.getAuthor = function () {
        return authorElement.text();
    }

    this.setDate = function (date) {
        let dateString = $.format.date(date, "dd.MM.yyyy");
        dateElement.text(dateString);
        bookDate = date;
    }

    {
        bookId = id;
        let element = LoadedFileElements.getBookTableRow();
        nameElement = element.find('.books-table__name');
        this.setName(name);
        authorElement = element.find('.books-table__author');
        this.setAuthor(author);
        dateElement = element.find('.books-table__date');
        this.setDate(date);
        let context = this;
        element.find('.books-table__edit').on('click', function () {
            let name = context.getName();
            let author = context.getAuthor();
            BookEditor.showToEdit(bookId, Page.getIsPublic(), name, author, bookDate);
        });
        element.find('.books-table__close').on('click', function () {
            let confirmation = confirm(Texts.DELETE_BOOK_CONFIRM_MSG);
            if (confirmation) {
                BookEditManager.delete(bookId, onDelete);
            }
        });
        container = element.find('tr');
    }
}
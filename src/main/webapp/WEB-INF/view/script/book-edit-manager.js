import { Texts } from "./texts.js";

function BookEditManagerClass() {
    this.getTexts = function (onDone) {
        $.post('/library/api/get-texts')
            .done(function (res) {
                onDone(res);
            })
            .fail(function () {
                alert(Texts.SERVER_CONNECTION_FAIL_MSG);
            });
    }
    this.get = function (isPublic, onDone) {
        $.post(Texts.GET_BOOKS_URL, { isPublic: isPublic })
            .done(function (res) {
                onDone(res);
            })
            .fail(function () {
                alert(Texts.SERVER_CONNECTION_FAIL_MSG);
            });
    }
    this.save = function (book, onSuccess, onComplete) {
        $.ajax({
            url: Texts.SAVE_BOOK_URL,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(book),
            processData: false,
            success: function () {
                onSuccess(book);
            },
            error: function () {
                alert(Texts.SAVE_BOOK_FAIL_MSG);
            },
            complete: function () {
                onComplete();
            }
        });
    }
    this.add = function (book, onSuccess, onComplete) {
        $.ajax({
            url: Texts.ADD_BOOK_URL,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(book),
            processData: false,
            success: function (book) {
                onSuccess(book);
            },
            error: function () {
                alert(Texts.ADD_BOOK_FAIL_MSG);
            },
            complete: function () {
                onComplete();
            }
        });
    }
    this.delete = function (id, onDone) {
        $.post(Texts.DELETE_BOOK_URL, { id: id })
            .done(function () {
                onDone();
            })
            .fail(function () {
                alert(Texts.DELETE_BOOK_FAIL_MSG);
            });
    }
}

export const BookEditManager = new BookEditManagerClass;
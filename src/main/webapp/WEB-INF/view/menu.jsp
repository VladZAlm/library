<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="ru">

<head>
	<meta charset="utf-8" />
	<title>Библиотека</title>
	<link rel="stylesheet" href="style/style.css" />
</head>

<body>
	<div class="catalog-header">Каталог библиотеки</div>
	<div class="catalog-type-buttons-container">
		<a id="public-catalog-button">
			<div class="catalog-type-button public-catalog-button">Публичный</div>
		</a>
		<a id="private-catalog-button">
			<div class="catalog-type-button private-catalog-button">Приватный</div>
		</a>
	</div>
	<div class="add-book-button">Добавить</div>
	<table class="books-table" style="display: none">
		<tr class="books-table__header">
			<th class="books-table__header-column">Название</th>
			<th class="books-table__header-column">Автор</th>
			<th class="books-table__header-column">Дата печати</th>
		</tr>
	</table>
</body>

</html>
<script src="script/jquery/jquery.js"></script>
<script src="script/jquery/date-format.js"></script>
<script type="module" src="script/init.js"></script>
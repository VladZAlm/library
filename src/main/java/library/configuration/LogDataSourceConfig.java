package library.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;

import library.utils.Constants;

public class LogDataSourceConfig {

	private static BasicDataSource dataSource;

	public static Connection getConnection() throws SQLException, IOException {
		return getDataSource().getConnection();
	}

	private static BasicDataSource getDataSource() throws IOException {
		if (dataSource != null) {
			return dataSource;
		}
		dataSource = new BasicDataSource();
		Properties properties = getProperties();
		dataSource.setDriverClassName(properties.getProperty(Constants.JDBC_DRIVER_CLASS_NAME));
		dataSource.setUrl(properties.getProperty(Constants.JDBC_URL));
		dataSource.setUsername(properties.getProperty(Constants.JDBC_USERNAME));
		dataSource.setPassword(properties.getProperty(Constants.JDBC_PASSWORD));
		return dataSource;
	}

	private static Properties getProperties() throws IOException {
		InputStream inputStream = LogDataSourceConfig.class.getClassLoader()
				.getResourceAsStream(Constants.PERSISTANCE_POSTGRES_PROPERTIES);
		Properties properties = new Properties();
		properties.load(inputStream);
		return properties;
	}
}

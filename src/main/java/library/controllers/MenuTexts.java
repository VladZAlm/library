package library.controllers;

public class MenuTexts {
	private String publicCatalogUrl;
	private String privateCatalogUrl;
	private String getBooksUrl;
	private String saveBookUrl;
	private String addBookUrl;
	private String deleteBookUrl;
	private String saveBookFailMsg;
	private String addBookFailMsg;
	private String deleteBookFailMsg;
	private String needBookNameMsg;
	private String needBookDateMsg;
	private String deleteBookConfirmMsg;
	private String editorEditHeader;
	private String editorAddHeader;

	public MenuTexts(String publicCatalogUrl, String privateCatalogUrl, String getBooksUrl, String saveBookUrl,
			String addBookUrl, String deleteBookUrl, String saveBookFailMsg, String addBookFailMsg,
			String deleteBookFailMsg, String needBookNameMsg, String needBookDateMsg, String deleteBookConfirmMsg,
			String editorEditHeader, String editorAddHeader) {
		this.publicCatalogUrl = publicCatalogUrl;
		this.privateCatalogUrl = privateCatalogUrl;
		this.getBooksUrl = getBooksUrl;
		this.saveBookUrl = saveBookUrl;
		this.addBookUrl = addBookUrl;
		this.deleteBookUrl = deleteBookUrl;
		this.saveBookFailMsg = saveBookFailMsg;
		this.addBookFailMsg = addBookFailMsg;
		this.deleteBookFailMsg = deleteBookFailMsg;
		this.needBookNameMsg = needBookNameMsg;
		this.needBookDateMsg = needBookDateMsg;
		this.deleteBookConfirmMsg = deleteBookConfirmMsg;
		this.editorEditHeader = editorEditHeader;
		this.editorAddHeader = editorAddHeader;
	}

	public String getPublicCatalogUrl() {
		return publicCatalogUrl;
	}

	public String getPrivateCatalogUrl() {
		return privateCatalogUrl;
	}

	public String getGetBooksUrl() {
		return getBooksUrl;
	}

	public String getSaveBookUrl() {
		return saveBookUrl;
	}

	public String getAddBookUrl() {
		return addBookUrl;
	}

	public String getDeleteBookUrl() {
		return deleteBookUrl;
	}

	public String getSaveBookFailMsg() {
		return saveBookFailMsg;
	}

	public String getAddBookFailMsg() {
		return addBookFailMsg;
	}

	public String getDeleteBookFailMsg() {
		return deleteBookFailMsg;
	}

	public String getNeedBookNameMsg() {
		return needBookNameMsg;
	}

	public String getNeedBookDateMsg() {
		return needBookDateMsg;
	}

	public String getDeleteBookConfirmMsg() {
		return deleteBookConfirmMsg;
	}

	public String getEditorEditHeader() {
		return editorEditHeader;
	}

	public String getEditorAddHeader() {
		return editorAddHeader;
	}
}

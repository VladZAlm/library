package library.controllers;

import org.springframework.web.util.UriComponentsBuilder;

public class Mappings {
	public static final String VIEW = "/view";
	public static final String MENU = "/menu";

	public static final String API = "/api";
	public static final String GET_BOOKS = "/get-books";
	public static final String ADD_BOOK = "/add-books";
	public static final String SAVE_BOOK = "/save-book";
	public static final String DELETE_BOOK = "/delete-book";

	public static String getUrl(UriComponentsBuilder ucb, String... mappings) {
		return ucb.toUriString() + String.join("", mappings);
	}
}

package library.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import library.exceptions.ExceptionToLog;
import library.exceptions.LibraryException;
import library.log.Logger;
import library.log.LoggerWrapper;
import library.log.SessionInfo;
import library.manager.TextsManager;
import library.persistance.entities.Book;
import library.persistance.services.BookService;

@RestController
@RequestMapping("/api")
public class RestApiController {
	private static final LoggerWrapper LOGGER = new LoggerWrapper(RestApiController.class);

	private static final HttpStatus SUCCESS_RESPONSE_STATUS = HttpStatus.OK;
	private static final HttpStatus ERROR_RESPONSE_STATUS = HttpStatus.BAD_REQUEST;

	@Autowired
	private BookService bookService;
	@Autowired
	private TextsManager textsManager;

	@PostMapping(value = Mappings.GET_BOOKS)
	@ResponseBody
	public List<Book> getBooks(@RequestParam boolean isPublic) {
		return bookService.getBooks(isPublic);
	}

	@PostMapping(value = Mappings.ADD_BOOK)
	public ResponseEntity<Book> addBook(@RequestBody Book book) {
		SessionInfo sessionInfo = new SessionInfo();
		Logger logger = LOGGER.createMethodLogger("addBook", sessionInfo);
		try {
			logBook(book, false, logger);
			Book res = bookService.addBook(book, sessionInfo);
			return createResponse(res, SUCCESS_RESPONSE_STATUS);
		} catch (Exception e) {
			handleException(e, logger);
			return createResponse(null, ERROR_RESPONSE_STATUS);
		}
	}

	@PostMapping(value = Mappings.SAVE_BOOK)
	public ResponseEntity<?> saveBook(@RequestBody Book book) {
		SessionInfo sessionInfo = new SessionInfo();
		Logger logger = LOGGER.createMethodLogger("saveBook", sessionInfo);
		try {
			logBook(book, true, logger);
			bookService.saveBook(book, sessionInfo);
			return createResponse(SUCCESS_RESPONSE_STATUS);
		} catch (Exception e) {
			handleException(e, logger);
			return createResponse(ERROR_RESPONSE_STATUS);
		}
	}

	@PostMapping(value = Mappings.DELETE_BOOK)
	public ResponseEntity<?> deleteBook(@RequestParam Long id) {
		SessionInfo sessionInfo = new SessionInfo();
		Logger logger = LOGGER.createMethodLogger("deleteBook", sessionInfo);
		logger.trace("id=%d", id);
		try {
			if (id == null) {
				throw new ExceptionToLog("Id is null");
			}
			bookService.deleteBook(id, sessionInfo);
			return createResponse(SUCCESS_RESPONSE_STATUS);
		} catch (Exception e) {
			handleException(e, logger);
			return createResponse(ERROR_RESPONSE_STATUS);
		}
	}

	@PostMapping(value = "/get-texts")
	public ResponseEntity<MenuTexts> getTexts(UriComponentsBuilder ucb) {
		SessionInfo sessionInfo = new SessionInfo();
		String publicCatalogUrl = ViewController.getPublicCatalogUrl(ucb);
		String privateCatalogUrl = ViewController.getPrivateCatalogUrl(ucb);
		String getBooksUrl = Mappings.getUrl(ucb, Mappings.API, Mappings.GET_BOOKS);
		String saveBookUrl = Mappings.getUrl(ucb, Mappings.API, Mappings.SAVE_BOOK);
		String addBookUrl = Mappings.getUrl(ucb, Mappings.API, Mappings.ADD_BOOK);
		String deleteBookUrl = Mappings.getUrl(ucb, Mappings.API, Mappings.DELETE_BOOK);
		MenuTexts texts = textsManager.getMenuTexts(publicCatalogUrl, privateCatalogUrl, getBooksUrl, saveBookUrl,
				addBookUrl, deleteBookUrl, sessionInfo);
		return createResponse(texts, SUCCESS_RESPONSE_STATUS);
	}

	private void logBook(Book book, boolean logId, Logger logger) {
		String name = book.getName();
		String author = book.getAuthor();
		Long releaseDate;
		if (book.getReleaseDate() == null) {
			releaseDate = null;
		} else {
			releaseDate = book.getReleaseDate().getTime();
		}
		String isPublic = String.valueOf(book.getIsPublic());
		String format = "name='%s',author='%s',releaseDate=%d,isPublic=%s";
		List<Object> args = Arrays.asList(name, author, releaseDate, isPublic);
		if (logId) {
			format = "id=%d," + format;
			Long id = book.getId();
			args = new ArrayList<>(args);
			args.add(0, id);
		}
		String message = String.format(format, args.toArray());
		logger.trace(message);
	}

	private ResponseEntity<Object> createResponse(HttpStatus status) {
		return new ResponseEntity<Object>(status);
	}

	private <T> ResponseEntity<T> createResponse(T body, HttpStatus status) {
		return new ResponseEntity<T>(body, status);
	}

	private void handleException(Exception e, Logger logger) {
		if (e instanceof ExceptionToLog) {
			logger.error(e.getMessage());
			return;
		}
		if (e instanceof LibraryException) {
			return;
		}
		logger.error(e);
	}
}
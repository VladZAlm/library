package library.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
@RequestMapping(Mappings.VIEW)
public class ViewController {
	private static final String PUBLIC_CATALOG = "public";
	private static final String PRIVATE_CATALOG = "private";

	private static final String CATALOG_PARAM_NAME = "catalog";

	public static String getPublicCatalogUrl(UriComponentsBuilder ucb) {
		return getCatalogUrl(ucb, PUBLIC_CATALOG);
	}

	public static String getPrivateCatalogUrl(UriComponentsBuilder ucb) {
		return getCatalogUrl(ucb, PRIVATE_CATALOG);
	}

	private static String getCatalogUrl(UriComponentsBuilder ucb, String catalogType) {
		String url = Mappings.getUrl(ucb, Mappings.VIEW, Mappings.MENU);
		return String.format("%s?%s=%s", url, CATALOG_PARAM_NAME, catalogType);
	}

	@GetMapping(value = Mappings.MENU)
	public String menu(@RequestParam(required = false, name = CATALOG_PARAM_NAME) String catalog,
			UriComponentsBuilder ucb) {
		String res;
		if (catalog != null && (catalog.equals(PUBLIC_CATALOG) || catalog.equals(PRIVATE_CATALOG))) {
			res = "menu";
		} else {
			res = "redirect:" + getPublicCatalogUrl(ucb);
		}
		return res;
	}
}
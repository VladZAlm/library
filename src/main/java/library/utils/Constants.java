package library.utils;

public class Constants {
	public static final String PERSISTANCE_POSTGRES_PROPERTIES = "persistance-postgres.properties";
	public static final String JDBC_DRIVER_CLASS_NAME = "jdbc.driver.class.name";
	public static final String JDBC_URL = "jdbc.url";
	public static final String JDBC_USERNAME = "jdbc.username";
	public static final String JDBC_PASSWORD = "jdbc.password";
}

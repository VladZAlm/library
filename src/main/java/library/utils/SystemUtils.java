package library.utils;

import java.util.UUID;

public class SystemUtils {
	public static String generateToken() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}

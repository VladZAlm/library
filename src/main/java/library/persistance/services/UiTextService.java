package library.persistance.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import library.log.Logger;
import library.log.LoggerWrapper;
import library.log.SessionInfo;
import library.persistance.entities.UiText;
import library.persistance.repositories.UiTextRepository;

@Service
public class UiTextService {
	private static final LoggerWrapper LOGGER = new LoggerWrapper(UiTextService.class);

	@Autowired
	private UiTextRepository repository;

	@Cacheable(cacheNames = "UI_TEXT", key = "#name")
	public String getUiText(String name, SessionInfo sessionInfo) {
		Logger logger = LOGGER.createMethodLogger("getUiText", sessionInfo);
		Optional<UiText> optional = repository.findById(name);
		if (!optional.isPresent()) {
			logger.error("Text not found. Name='" + name + "'");
			return null;
		}
		return optional.get().getValue();
	}
}
package library.persistance.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import library.exceptions.ExceptionToLog;
import library.exceptions.LibraryException;
import library.log.Logger;
import library.log.LoggerWrapper;
import library.log.SessionInfo;
import library.persistance.entities.Book;
import library.persistance.repositories.BookRepository;
import library.utils.StringUtils;

@Service
public class BookService {
	private static final LoggerWrapper LOGGER = new LoggerWrapper(BookService.class);

	@Autowired
	private BookRepository repository;

	public List<Book> getBooks(boolean isPublic) {
		return repository.findByIsPublicOrderByName(isPublic);
	}

	@Transactional(rollbackFor = LibraryException.class)
	public Book addBook(Book book, SessionInfo sessionInfo) throws LibraryException {
		Logger logger = LOGGER.createMethodLogger("addBook", sessionInfo);
		try {
			validateBook(book, logger);
			return repository.save(book);
		} catch (Exception e) {
			if (!(e instanceof LibraryException)) {
				logger.error(e);
			}
			throw new LibraryException();
		}
	}

	@Transactional(rollbackFor = LibraryException.class)
	public void saveBook(Book book, SessionInfo sessionInfo) throws LibraryException {
		Logger logger = LOGGER.createMethodLogger("saveBook", sessionInfo);
		try {
			validateBook(book, logger);
			Optional<Book> optional = repository.findById(book.getId());
			if (!optional.isPresent()) {
				throw new ExceptionToLog("Book is not found. Id=" + book.getId());
			}
			repository.save(book);
		} catch (Exception e) {
			if (!(e instanceof LibraryException)) {
				logger.error(e);
			}
			throw new LibraryException();
		}
	}

	@Transactional(rollbackFor = LibraryException.class)
	public void deleteBook(Long id, SessionInfo sessionInfo) throws LibraryException {
		Logger logger = LOGGER.createMethodLogger("deleteBook", sessionInfo);
		try {
			repository.deleteById(id);
		} catch (Exception e) {
			logger.error(e);
			throw new LibraryException();
		}
	}

	private void validateBook(Book book, Logger logger) throws LibraryException {
		try {
			if (StringUtils.isBlank(book.getName())) {
				throw new ExceptionToLog("Name is blank");
			}
			if (StringUtils.isBlank(book.getAuthor())) {
				book.setAuthor(null);
			}
			if (book.getReleaseDate() == null) {
				throw new ExceptionToLog("ReleaseDate is null");
			}
			if (book.getIsPublic() == null) {
				throw new ExceptionToLog("IsPublic is null");
			}
		} catch (ExceptionToLog e) {
			logger.error(e.getMessage());
			throw new LibraryException();
		}
	}
}

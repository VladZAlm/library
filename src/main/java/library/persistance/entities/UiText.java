package library.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "UiText")
@Table(name = "ui_text")
public class UiText {
	@Id
	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "value", length = 1000, nullable = false)
	private String value;

	public UiText() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

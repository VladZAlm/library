package library.persistance.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import library.persistance.entities.UiText;

public interface UiTextRepository extends JpaRepository<UiText, String> {
}

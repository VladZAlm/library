package library.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import library.controllers.MenuTexts;
import library.log.SessionInfo;
import library.persistance.services.UiTextService;

@Component
public class TextsManager {
	@Autowired
	private UiTextService uiTextService;

	public MenuTexts getMenuTexts(String publicCatalogUrl, String privateCatalogUrl, String getBooksUrl,
			String saveBookUrl, String addBookUrl, String deleteBookUrl, SessionInfo sessionInfo) {
		String saveBookFailMsg = uiTextService.getUiText("save.book.fail.msg", sessionInfo);
		String addBookFailMsg = uiTextService.getUiText("add.book.fail.msg", sessionInfo);
		String deleteBookFailMsg = uiTextService.getUiText("delete.book.fail.msg", sessionInfo);
		String needBookNameMsg = uiTextService.getUiText("need.book.name.msg", sessionInfo);
		String needBookDateMsg = uiTextService.getUiText("need.book.date.msg", sessionInfo);
		String deleteBookConfirmMsg = uiTextService.getUiText("delete.book.confirm.msg", sessionInfo);
		String editorEditHeader = uiTextService.getUiText("editor.edit.header", sessionInfo);
		String editorAddHeader = uiTextService.getUiText("editor.add.header", sessionInfo);
		return new MenuTexts(publicCatalogUrl, privateCatalogUrl, getBooksUrl, saveBookUrl, addBookUrl, deleteBookUrl,
				saveBookFailMsg, addBookFailMsg, deleteBookFailMsg, needBookNameMsg, needBookDateMsg,
				deleteBookConfirmMsg, editorEditHeader, editorAddHeader);
	}
}

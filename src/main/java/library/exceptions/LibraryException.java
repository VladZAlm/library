package library.exceptions;

public class LibraryException extends Exception {

	private static final long serialVersionUID = 4589880786631407413L;

	public LibraryException() {
		super();
	}

	public LibraryException(String msg) {
		super(msg);
	}
}

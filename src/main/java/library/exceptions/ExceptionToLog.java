package library.exceptions;

public class ExceptionToLog extends LibraryException {

	private static final long serialVersionUID = 1419205020373912330L;

	public ExceptionToLog(String msg) {
		super(msg);
	}

}
